# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $("body").on "click", "#cancelar-form", ->
    $("#col_form").empty().removeClass("col-md-6");
    $("#col_index").removeClass("col-md-6").addClass("col-md-12");
    $("#link_new_person").removeClass("disabled");