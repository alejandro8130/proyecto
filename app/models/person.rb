class Person < ApplicationRecord
  validates :name, :lastname, presence: true
    
  def fullname
    "#{self.name} #{self.lastname}"
  end
end
